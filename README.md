## ASP .NET Core MVC Lista de tarefas
### Este é um projeto gerenciador de tarefas desenvolvido em ASP .NET Core MVC com alguns conceitos de Razor. 
- Ele permite que você crie, leia, atualize e delete tarefas.

## Funcionalidades
CRUD de Tarefas: Adicione, visualize, edite e delete.
- Tarefas estas que são definidas por Id, Titulo, descrição e status.
 - Armazenamento em JSON: As tarefas são armazenadas em um arquivo JSON  (System.Text.Json).
